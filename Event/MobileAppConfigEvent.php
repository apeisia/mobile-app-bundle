<?php
namespace Apeisia\MobileAppBundle\Event;

class MobileAppConfigEvent
{

    private array $config = [];

    public function setConfig(string $key, $value)
    {
        $this->config[$key] = $value;
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}
