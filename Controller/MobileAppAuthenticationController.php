<?php

namespace Apeisia\MobileAppBundle\Controller;

use Apeisia\BaseBundle\Generators\Random;
use Apeisia\ClientGeneratorBundle\Annotation\ClientRequestParameter;
use Apeisia\ClientGeneratorBundle\Annotation\GenerateClient;
use Apeisia\MobileAppBundle\Entity\MobileAppAuthToken;
use Apeisia\MobileAppBundle\Event\MobileAppConfigEvent;
use AppBundle\Entity\Login;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @Rest\NamePrefix("mobile_app_authentication_")
 * @Rest\Prefix("/mobile-auth")
 */
class MobileAppAuthenticationController extends AbstractController
{

    /**
     * @Rest\Post("/login")
     * @GenerateClient()
     * @ClientRequestParameter(name="username", type="string")
     * @ClientRequestParameter(name="password", type="string")
     * @ClientRequestParameter(name="deviceName", type="string")
     * @ClientRequestParameter(name="pushToken", type="string", optional=true)
     *
     * @param Request $request
     * @param EncoderFactoryInterface $encoderFactory
     * @return View|array
     * @throws Exception
     */
    public function loginAction(Request $request, EncoderFactoryInterface $encoderFactory)
    {
        $username   = $request->get('username');
        $password   = $request->get('password');
        $deviceName = $request->get('deviceName');

        /** @var Login|null $login */
        $login = $this->getDoctrine()->getRepository(Login::class)->findOneBy(['username' => $username]);

        if (!$login) {
            return View::create(null, Response::HTTP_UNAUTHORIZED);
        }

        $encoder = $encoderFactory->getEncoder($login);
        if (!$encoder->isPasswordValid($login->getPassword(), $password, $login->getSalt())) {
            return View::create(null, Response::HTTP_UNAUTHORIZED);
        }

        if (!$login->isEnabled()) {
            return View::create([
                'error'  => 'User is not enabled',
                'userId' => $login->getId()
            ], Response::HTTP_FORBIDDEN);
        }

        $token = new MobileAppAuthToken($login);
        $token->setDeviceName($deviceName);
        $token->setAppVersion($request->headers->get('X-App-Version'));

        $token->setLastIP($request->getClientIp());
        $token->setToken(Random::alphanumeric(50));
        $token->setLastAccess(new DateTime());
        $token->setFirebasePushToken($request->get('pushToken'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($token);
        $em->flush();

        return [
            'token'   => $token->getToken(),
            'loginId' => $login->getId(),
        ];
    }

    /**
     * @Rest\Post("/logout")
     * @GenerateClient()
     * @ClientRequestParameter(name="token", type="string")
     *
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return View
     */
    public function logoutAction(Request $request): View
    {
        $token = $this->getDoctrine()->getRepository(MobileAppAuthToken::class)->findOneBy([
            'token' => $request->get('token'),
        ]);

        if (!$token || $token->getLogin()->getId() !== $this->getUser()->getId()) {
            return View::create(null, Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($token);
        $em->flush();

        return View::create(null, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/push-token")
     * @GenerateClient()
     * @ClientRequestParameter(name="token", type="string")
     * @ClientRequestParameter(name="pushToken", type="string")
     *
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return View
     */
    public function setPushTokenAction(Request $request): View
    {
        $token = $this->getDoctrine()->getRepository(MobileAppAuthToken::class)->findOneBy([
            'token' => $request->get('token'),
        ]);

        if (!$token || $token->getLogin()->getId() !== $this->getUser()->getId()) {
            return View::create(null, Response::HTTP_NOT_FOUND);
        }
        $token->setFirebasePushToken($request->get('pushToken'));
        $this->getDoctrine()->getManager()->flush();

        return View::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Get("/config.json")
     * @Rest\View(serializerGroups={"mobile-config", "id"})
     * @GenerateClient()
     *
     * @IsGranted("ROLE_USER")
     * @param EventDispatcherInterface $eventDispatcher
     * @return array
     */
    public function configAction(EventDispatcherInterface $eventDispatcher): array
    {
        $ev = new MobileAppConfigEvent();
        $eventDispatcher->dispatch($ev);
        return $ev->getConfig();
    }

}
