<?php

namespace Apeisia\MobileAppBundle\Entity;

use Apeisia\AccessorTraitBundle\Annotation\GetSet;
use Apeisia\BaseBundle\Entity\EntityUUId;
use Apeisia\MobileAppBundle\Entity\AccessorTrait\MobileAppAuthTokenAccessors;
use AppBundle\Entity\Login;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="Apeisia\MobileAppBundle\Repository\MobileAppAuthTokenRepository")
 * @GetSet
 */
class MobileAppAuthToken
{
    use EntityUUId;
    use MobileAppAuthTokenAccessors;

    /**
     * @ORM\ManyToOne(targetEntity=Login::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Login $login;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Serializer\Groups({"default", "list"})
     */
    private ?string $token = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Serializer\Groups({"default", "list"})
     */
    private ?DateTime $createdAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Groups({"default", "list"})
     */
    private ?string $appVersion = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Groups({"default", "list"})
     */
    private ?string $deviceName = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Serializer\Groups({"default", "list"})
     */
    private ?string $lastIP = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Serializer\Groups({"default", "list"})
     */
    private ?DateTime $lastAccess = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $firebasePushToken;

    public function __construct(?Login $login = null)
    {
        $this->createdAt = new DateTime();
        $this->login     = $login;
    }

}
