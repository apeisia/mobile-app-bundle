<?php
namespace Apeisia\MobileAppBundle\Entity\AccessorTrait;

use AppBundle\Entity\Login;

/**
 * Generated accessor trait. Do not edit.
 * @Apeisia\AccessorTraitBundle\Annotation\Generated
 */
trait MobileAppAuthTokenAccessors
{
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return Login
     */
    public function getLogin(): Login
    {
        return $this->login;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param Login $login
     * @return self
     */
    public function setLogin(Login $login): self
    {
        $this->login = $login;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param string|null $token
     * @return self
     */
    public function setToken(?string $token): self
    {
        $this->token = $token;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param ?\DateTimeInterface $createdAt
     * @return self
     */
    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        if ($createdAt instanceof \DateTimeImmutable) {
            $createdAt = \DateTime::createFromImmutable($createdAt);
        }
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return string|null
     */
    public function getAppVersion(): ?string
    {
        return $this->appVersion;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param string|null $appVersion
     * @return self
     */
    public function setAppVersion(?string $appVersion): self
    {
        $this->appVersion = $appVersion;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return string|null
     */
    public function getDeviceName(): ?string
    {
        return $this->deviceName;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param string|null $deviceName
     * @return self
     */
    public function setDeviceName(?string $deviceName): self
    {
        $this->deviceName = $deviceName;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return string|null
     */
    public function getLastIP(): ?string
    {
        return $this->lastIP;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param string|null $lastIP
     * @return self
     */
    public function setLastIP(?string $lastIP): self
    {
        $this->lastIP = $lastIP;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return \DateTime|null
     */
    public function getLastAccess(): ?\DateTime
    {
        return $this->lastAccess;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param ?\DateTimeInterface $lastAccess
     * @return self
     */
    public function setLastAccess(?\DateTimeInterface $lastAccess): self
    {
        if ($lastAccess instanceof \DateTimeImmutable) {
            $lastAccess = \DateTime::createFromImmutable($lastAccess);
        }
        $this->lastAccess = $lastAccess;
        
        return $this;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @return string|null
     */
    public function getFirebasePushToken(): ?string
    {
        return $this->firebasePushToken;
    }
    
    /**
     * Generated accessor. Do not edit.
     *
     * @Apeisia\AccessorTraitBundle\Annotation\Generated
     * @param string|null $firebasePushToken
     * @return self
     */
    public function setFirebasePushToken(?string $firebasePushToken): self
    {
        $this->firebasePushToken = $firebasePushToken;
        
        return $this;
    }
}
