<?php

namespace Apeisia\MobileAppBundle\Repository;

use Apeisia\BaseBundle\Crud\AbstractSearchRepository;
use Apeisia\LoginAccess\Entity\Account;
use Apeisia\LoginAccess\Entity\Login;
use Apeisia\MobileAppBundle\Entity\MobileAppAuthToken;

class MobileAppAuthTokenRepository extends AbstractSearchRepository
{
    public function findOneByToken(string $token): ?MobileAppAuthToken
    {
        return $this->findOneBy(['token' => $token]);
    }

    protected function getSearchFields(): array
    {
        return [];
    }

    /**
     * @param Login $login
     * @return MobileAppAuthToken
     */
    public function findPushForLogin(Login $login)
    {
        return $this->createQueryBuilder('a')
            ->where('a.login = :login')
            ->andWhere('a.firebasePushToken IS NOT NULL')
            ->setParameter('login', $login)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Login $account
     * @return MobileAppAuthToken
     */
    public function findPushForAccount(Account $account)
    {
        return $this->createQueryBuilder('a')
            ->join('a.login', 'l')
            ->join('l.accesses', 'accesses')
            ->where('accesses.account = :account')
            ->andWhere('a.firebasePushToken IS NOT NULL')
            ->setParameter('account', $account)
            ->getQuery()
            ->getResult();
    }

}
