<?php

namespace Apeisia\MobileAppBundle\Security;

use Apeisia\MobileAppBundle\Entity\MobileAppAuthToken;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class MobileAppTokenGuardAuthenticator extends AbstractGuardAuthenticator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports(Request $request)
    {
        return ($request->headers->has('Authorization')
            && Str::startsWith($request->headers->get('Authorization'), 'Bearer ')
            && $request->headers->has('X-App-Version')) || $request->query->has('_app_token');
    }

    public function getCredentials(Request $request)
    {
        return [
            'token'   => $request->query->has('_app_token') ? $request->query->get('_app_token') : substr($request->headers->get('Authorization'), strlen('Bearer ')),
            'version' => $request->headers->get('X-App-Version')
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if ($credentials === null) {
            return null;
        }

        /** @var MobileAppAuthToken $token */
        $token = $this->entityManager->getRepository(MobileAppAuthToken::class)->findOneBy([
            'token' => $credentials['token']
        ]);

        if (!$token) {
            return null;
        }

        $this->currentAuthToken = $token;
        if($credentials['version']) {
            $token->setAppVersion($credentials['version']);
            // todo this is probably not ideal, but doctrine removed support for flushing single entities
            $this->entityManager->flush();
        }

        return $token->getLogin();
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        //return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(['message' => 'Authentication required.'], Response::HTTP_UNAUTHORIZED);
    }
}
