<?php

namespace Apeisia\MobileAppBundle\Service;

use Apeisia\MobileAppBundle\Entity\MobileAppAuthToken;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\MessageTarget;
use Psr\Log\LoggerInterface;

class PushService
{
    private Messaging $messaging;
    private LoggerInterface $logger;

    public function __construct(Messaging $messaging, LoggerInterface $logger)
    {
        $this->messaging = $messaging;
        $this->logger    = $logger;
    }

    public function sendNotificationToToken(MobileAppAuthToken $authToken, $title, $body, $data = [])
    {
        try {
            $message = CloudMessage::withTarget(MessageTarget::TOKEN, $authToken->getFirebasePushToken());
            $message = $message->withData($data)->withHighestPossiblePriority();
            $message = $message->withNotification([
                'title' => $title,
                'body'  => $body,
            ]);
            $this->logger->info('[Push] Notification to {regToken} "{title}" "{body}")', [
                'regToken' => $authToken->getFirebasePushToken(),
                'title'    => $title,
                'body'     => $body,
            ]);

            try {
                $this->messaging->send($message);
            } catch (InvalidMessage $e) {
                $this->logger->warning('[Push] {error}', [
                    'error' => $e->getMessage(),
                    'token' => $authToken->getFirebasePushToken(),
                ]);
            }
        } catch (NotFound $e) {
            // entity not found -> token invalid
            $authToken->setFirebasePushToken(null);
            $this->logger->warning('[Push] token invalid');
        }
    }

    public function sendDataToToken(MobileAppAuthToken $authToken, $data = [], $highPriority = false)
    {
        try {
            $message = CloudMessage::withTarget(MessageTarget::TOKEN, $authToken->getFirebasePushToken());
            $message = $message->withData($data);
            $message = $message->withAndroidConfig([
                'priority' => $highPriority ? 'high' : 'normal',
            ]);

            $this->logger->info('[Push] Data ({priority}) to  {regToken}: {data}', [
                'regToken' => $authToken->getFirebasePushToken(),
                'priority' => ($highPriority ? 'high' : 'normal'),
                'data'     => json_encode($data),
            ]);
            try {
                $this->messaging->send($message);
            } catch (InvalidMessage $e) {
                $this->logger->warning('[Push] {error}', [
                    'error' => $e->getMessage(),
                ]);
            }
        } catch (NotFound $e) {
            // entity not found -> token invalid
            $authToken->setFirebasePushToken(null);
            $this->logger->warning('[Push] token invalid');
        }
    }

}
